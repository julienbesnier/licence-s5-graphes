CC = gcc -std=c11 -Wpedantic -D_XOPEN_SOURCE -Wall -Wextra -Wconversion -Werror

objects = *.o

all: plateau_jeu

clean:
	$(RM) $(objects) *.so

plateau_jeu: plateau_jeu.o

plateau_jeu.o: plateau_jeu.c plateau_jeu.h
