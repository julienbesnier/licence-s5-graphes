#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

#include "canmove.h"

bool is_free(int x, int y, int tab[LINE_NB][COL_NB]) {
    if (tab[x][y] == LIBRE) {
        return true;
    }

    return false;
}

bool exist(int x, int y) {
    if (x > 6 || x < 0 || y > 6 || y < 0) {
        return false;
    }
    if ((x <= 1 || 5 <= x) && (y <= 1 || 5 <= y)) {
        return false;
    }

    return true;
}

void coo_move(int x, int y,
  int *a, int *b, int *c, int *d, int *e, int *f, int dir) {
    *a = x;
    *b = y;
    if (dir == NORTH) {
        *c = x - 1;
        *d = y;
        *e = x - 2;
        *f = y;
    } else if (dir == EAST) {
        *c = x;
        *d = y + 1;
        *e = x;
        *f = y + 2;
    } else if (dir == SOUTH) {
        *c = x + 1;
        *d = y;
        *e = x + 2;
        *f = y;
    } else {
        // NB : dir == WEST
        *c = x;
        *d = y - 1;
        *e = x;
        *f = y - 2;
    }
}

int which_dir(int x, int y, int x2, int y2) {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
    for (int i = 1; i <= 4; ++i) {
        coo_move(x, y, &a, &b, &c, &d, &e, &f, i);
        if (e == x2 && f == y2) {
            return i;
        }
    }

    return -1;
}

bool can_move(int x, int y, int tab[LINE_NB][COL_NB]) {
    return can_move_in(x, y, NORTH, tab)
      || can_move_in(x, y, EAST, tab)
      || can_move_in(x, y, SOUTH, tab)
      || can_move_in(x, y, WEST, tab);
}

bool can_move_in(int x, int y, int dir, int tab[LINE_NB][COL_NB]) {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
    coo_move(x, y, &a, &b, &c, &d, &e, &f, dir);
    return exist(a, b) && !is_free(a, b, tab)
          && exist(c, d) && !is_free(c, d, tab)
          && exist(e, f) && is_free(e, f, tab);
}
