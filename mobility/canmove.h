#include <stdbool.h>

#include "info_plateau.h"

#ifndef CANMOVE_H
#define CANMOVE_H

//  is_free : fonction qui teste la vacuité d'une case du plateau.
//    renvoi true si la case est vide, false sinon.
extern bool is_free(int x, int y, int tab[LINE_NB][COL_NB]);

//  exist : fonction qui indique si la coordonnées (x, y) est une case
//    valide du plateau.
extern bool exist(int x, int y);

//  coo_move : fonction qui calcule les coordonnées (a,b) = (x,y) du point
//    de départ du piont, (c,d) du pion sur lequel on saute et (e,f) d'arrivée
//    du pion sachant la direction dir. Ne s'occupe pas de l'existence
//    de ces points sur le plateau.
extern void coo_move(int x, int y, int *a, int *b, int *c, int *d, int *e, int *f, int dir);

//  which_dir : fonction qui calcule la direction qu'il faut emprunter
//    pour aller de la coordonnée (x,y) vers la case de coordonnée (x2, y2).
//    Retourne -1 en cas d'échec.
extern int which_dir(int x, int y, int x2, int y2);

//  can_move : fonction qui indique si le pion de coordonnées (x, y)
//    peut se déplacer dans au moins une directions.
extern bool can_move(int x, int y, int tab[LINE_NB][COL_NB]);

//  can_move_in : fonction qui indique si le pion de coordonnées
//    (x, y) peut se déplacer dans la direction dir.
extern bool can_move_in(int x, int y, int dir, int tab[LINE_NB][COL_NB]);

#endif  // CANMOVE_H
