#define LINE_NB 7
#define COL_NB 7

#define LIBRE 1
#define OCCUPE 0

// définition des entiers qui correspondent aux directions
#define NORTH 1
#define EAST 2
#define SOUTH 3
#define WEST 4
