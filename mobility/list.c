#include <stdlib.h>
#include <stdio.h>
#include "list.h"


typedef struct cslist cslist;

struct cslist {
  int value1;
  int value2;
  int direction;
  cslist *next;
};

struct slist {
  cslist *head;
};


slist *slist_empty(void) {
  slist *list = malloc(sizeof *list);
  if (list == NULL) {
    return NULL;
  }
  list -> head = NULL;
  return list;
}

bool slist_isempty(slist *s) {
    if ((s -> head) == NULL) {
        return true;
    } else {
        return false;
    }
}

void *slist_insert_head(slist *list, int x, int y, int d) {
  cslist *p = malloc(sizeof *p);
  if (p == NULL) {
    return NULL;
  }
  p -> value1 = x;
  p -> value2 = y;
  p -> direction = d;
  p -> next = list -> head;
  list -> head = p;
  return p;
}

void *slist_head_values(slist *list, int *x, int *y, int *d) {
  cslist *p = list -> head;
  if (p != NULL) {
      *x = p -> value1;
      *y = p -> value2;
      *d = p -> direction;
  }
  return p;
}

void *slist_remove_head(slist *list) {
  cslist *p = list -> head;
  list -> head = p -> next;
  free(p);
  return list;
}

void slist_dispose(slist **ptrlist) {
  if (*ptrlist != NULL) {
    cslist *p = (*ptrlist) -> head;
    while (p != NULL) {
      cslist *t = p;
      p = p -> next;
      free(t);
    }
    free(*ptrlist);
    *ptrlist = NULL;
  }
}

void slist_print(slist *list) {
    if (!slist_isempty(list)) {
        cslist *p = list -> head;
        int n = 1;
        printf("État de la liste de possibilités :\n");
        while (p != NULL) {
          printf("- Cellule n° %d\n", n);
          printf("x : %d\n", p -> value1);
          printf("y : %d\n", p -> value2);
          printf("direction : ");

          switch(p -> direction) {
          case 1:
            printf("NORTH\n");
            break;
          case 2:
            printf("EST\n");
            break;
          case 3:
            printf("SOUTH\n");
            break;
          default:
            printf("WEST\n");
          }

          p = p -> next;
          n = n + 1;
        }
    }
}
