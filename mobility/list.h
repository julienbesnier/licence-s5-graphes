#include <stdbool.h>

typedef struct slist slist;

#ifndef LIST_H
#define LIST_H


//  slist_empty : crée une liste dynamique simplement chainée sur le type
//    void * initialement vide. Renvoie un pointeur sur l'objet qui gère cette
//    liste en cas de succès et NULL en cas d'échec
extern slist *slist_empty(void);

//  slist_isempty : renvoie true si la liste associée à s est
//    vide ou false sinon.
extern bool slist_isempty(slist *s);

//  slist_insert_head : insère x  et y en tête de la liste associée à list.
extern void *slist_insert_head(slist *list, int x, int y, int d);

//  slist_head_values : si la liste n'est pas nulle, initialise *x et *y
//    aux valeurs de values1 et values2 de la cellule en tête de la liste.
extern void *slist_head_values(slist *list, int *x, int *y, int *d);

//  slist_remove_head : retire le premier élément en tête et le retourne.
extern void *slist_remove_head(slist *list);

//  slist_dispose : libère les ressources allouées à *ptrlist ainsi qu'à la liste
//    associée et donne à *ptrlist la valeur NULL. Tolère que *ptrlist vaille NULL
extern void slist_dispose(slist **ptrlist);

//  slist_print : affiche la liste list.
extern void slist_print(slist *list);

#endif  // LIST_H
