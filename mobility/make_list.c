#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "make_list.h"
#include "list.c"

slist *make_list(int tab[LINE_NB][COL_NB]) {
  slist *list = slist_empty();
  for (int x = 0; x < LINE_NB ; ++x) { // pour tous les pions placés sur le plateau
    for (int y = 0 ; y < COL_NB ; ++y) {
      for (int dir = 1; dir <= 4; ++dir) {
        if (can_move_in(x, y, dir, tab)) {
          slist_insert_head(list, x, y, dir);
        }
      }
    }
  }

  return list;
}
