#include <stdlib.h>

#include "canmove.c"

#ifndef MAKE_LIST_H
#define MAKE_LIST_H

typedef struct slist slist;

//  makeList : crée une liste des coordonnées des pièces qui peuvent bouger.
extern slist *make_list(int tab[LINE_NB][COL_NB]);

#endif  // MAKE_LIST_H
