#include <stdlib.h>
#include <stdio.h>
#include "movements.h"


typedef struct cmovlist cmovlist;

struct cmovlist {
    int x1;
    int y1;
    int x2;
    int y2;
    cmovlist *next;
};

struct movlist {
    cmovlist *head;
};

movlist *movlist_empty(void) {
    movlist *s = malloc(sizeof *s);
    if (s == NULL) {
        return NULL;
    }
    s -> head = NULL;

    return s;
}

bool movlist_isempty(movlist *s) {
    if ((s -> head) == NULL) {
        return true;
    } else {
        return false;
    }
}

int movlist_head_values(movlist *s, int *x1, int *y1, int *x2, int *y2) {
    if (movlist_isempty(s)) {
        return -1;
    }
    *x1 = (s -> head -> x1);
    *y1 = (s -> head -> y1);
    *x2 = (s -> head -> x2);
    *y2 = (s -> head -> y2);

    return 0;
}

int movlist_insert_head(movlist *s, int x1, int y1, int x2, int y2) {
    // on crée une nouvelle cellule
    cmovlist *p = malloc(sizeof *p);
    if (p == NULL) {
        return -1;
    }
    // on l'initialise
    p -> x1 = x1;
    p -> y1 = y1;
    p -> x2 = x2;
    p -> y2 = y2;
    // on l'insère en tête
    p -> next = s -> head;
    s -> head = p;

    return 0;
}

int movlist_remove_head(movlist *s) {
    if (movlist_isempty(s)) {
        return -1;
    }
    cmovlist *h = s -> head;
    s -> head = h -> next;
    free(h);

    return 0;
}

void movlist_dispose(movlist **ptrlist) {
  if (*ptrlist != NULL) {
    cmovlist *p = (*ptrlist) -> head;
    while (p != NULL) {
      cmovlist *t = p;
      p = p -> next;
      free(t);
    }
    free(*ptrlist);
    *ptrlist = NULL;
  }
}

void movlist_print(movlist *list) {
    if (!movlist_isempty(list)) {
        cmovlist *p = list -> head;
        movlist *list2 = movlist_empty();
        while (p != NULL) {
          movlist_insert_head(list2, p -> x1, p -> y1, p -> x2, p -> y2);
          p = p -> next;
        }

        cmovlist *p2 = list2 -> head;
        int n = 1;
        printf("Voici la liste de tous les mouvements effectués :\n");
        while (p2 != NULL) {
          printf("- Mouvement n° %d :\n", n);
          printf("(%d,%d)", p2 -> x1, p2 -> y1);
          printf(" vers ");
          printf("(%d,%d)\n", p2 -> x2, p2 -> y2);
          p2 = p2 -> next;
          n = n + 1;
        }
    }
}

int compute_movement_number(movlist *list) {
  if (!movlist_isempty(list)) {
      cmovlist *p = list -> head;
      int n = 1;
      // on initialise x et y à O car il n'y a pas de pion dans les coins
      int x = 0;
      int y = 0;
      while (p != NULL) {
        if ((x != (p -> x1)) && (y != (p -> x2))) {
          n = n + 1;
        }
        x = p -> x2;
        y = p -> y2;
        p = p -> next;
      }

      return n;
    }

    return 0;
}
