#include <stdbool.h>

typedef struct movlist movlist;

#ifndef MOVEMENTS_H
#define MOVEMENTS_H

//  movlist_empty : fonction qui crée un objet qui gère une liste dynamique
//    simplement chainée sur le type movlist. La liste est vide.
//    Renvoie un pointeur vers l'objet en cas de succès, NULL en cas d'échec.
extern movlist *movlist_empty(void);

//  movelist_isempty : renvoie true si la liste associée à s est
//    vide ou false sinon.
extern bool movlist_isempty(movlist *s);

//  movlist_head_value : si la liste associée à s n'est pas vide, affecte à *x1, *y1, *x2 et *y2
//    les valeurs des différents champs de l'élément de tête et renvoie zéro.
//    Renvoie une valeur non nulle sinon
extern int movlist_head_values(movlist *s, int *x1, int *y1, int *x2, int *y2);

//  movlist_insert_head : insère une copie de *x1, *y1, *x2 et *y2
//    en tête de la liste associée à s.
//    Renvoie zéro en cas de succès, une valeur non nulle en cas d'échec.
extern int movlist_insert_head(movlist *s, int x1, int y1, int x2, int y2);

// movlist_remove_head : supprime l'élément en tête de la liste associée à s.
//    Renvoie zéro en cas de succèrs, une valeur non nulle en cas d'échec.
extern int movlist_remove_head(movlist *s);

//  slist_dispose : libère les ressources allouées à *ptrlist ainsi qu'à la liste
//    associée et donne à *ptrlist la valeur NULL. Tolère que *ptrlist vaille NULL
extern void movlist_dispose(movlist **ptrlist);

//  movlist_print : affiche la liste list.
extern void movlist_print(movlist *list);

//  compute_movement_number : calcule le nombre de mouvements de la liste
//    à l'instant t. Pour rappel, si un pion se déplace plusieurs fois d'affilé,
//    un seul coup est compté pour l'ensemble de ses mouvements contigus.
extern int compute_movement_number(movlist *list);

#endif  // MOVEMENTS_H
