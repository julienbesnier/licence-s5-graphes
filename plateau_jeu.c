#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <time.h>

#include "plateau_jeu.h"


//--- fonction principale

#define ARG__OPT_LONG "--"
#define ARG_HELP      "help"

int main(int argc, char *argv[]) {
  // test de l'existence d'option(s)
  for (int k = 1; k < argc; ++k) {
    const char *a = argv[k];
    if (strlen(a) > strlen(ARG__OPT_LONG)
        && strlen(a) <= strlen(ARG__OPT_LONG ARG_HELP)
        && strncmp(a, ARG__OPT_LONG ARG_HELP, strlen(a)) == 0) {
      aide(argv[0]);
      return EXIT_SUCCESS;
    }
  }

  start_backtraking();

  printf("\nLe programme a mis %f secondes à s'exécuter.\n\n",
      (double) clock()/CLOCKS_PER_SEC);

  return EXIT_SUCCESS;
}


//--- aide
#define WELCOME "Bienvenue dans l'aide du jeu de solitaire chinois. "   \


#define INIT "D'abbord, vous serez invité à choisir un mode de jeu. "   \
"Ils sont au nombre de quatre :"                                        \

#define BASIC "Ce mode de jeu va vous permettre de choisir "            \
"la découverte d'un chemin, ainsi que l'affichage de sa longueur, "     \
"mais aussi de la suite des mouvements des pions qui le composent."     \

#define AVANCE "Ce mode de jeu est un peu plus complet. "               \
"Il va vous permettre de choisir la découverte du plus court chemin, "  \
"parmi les n premiers découverts ainsi que l'affichage "                \
"de sa longueur, mais aussi de la suite des mouvements des pions "      \
"qui le composent."                                                     \

#define EXPERT "Ce mode de jeu est le plus complet. "                   \
"Il va vous permettre de choisir la découverte du plus court chemin, "  \
"parmi toutes les possibilités ainsi que l'affichage de sa longueur, "  \
"mais aussi de la suite des mouvements des pions qui le composent."     \

#define EXPERT2 "Ce mode de jeu est le plus complet. "                  \
"Il va vous permettre de choisir la découverte du plus court chemin, "  \
"parmi toutes les possibilités ainsi que l'affichage de sa longueur, "  \
"mais aussi de la suite des mouvements des pions qui le composent."     \
"Le plateau est différent, le parcours est commencé, le parcours "      \
"est plus court."                                                       \

#define INIT2 "Ensuite, vous serez invité à choisir un profil de jeu. " \
"Ils sont au nombre de trois :"                                         \

#define CHEVALIER "Ce profil est le plus simple. Il vous permet "       \
"de lancer le jeu sans choisir de paramètre : le jeu commencera avec "  \
"la case libre au milieu et se terminera n'importe où"                  \

#define SEIGNEUR "Vous être cette fois ci un Seigneur. Vous pouvez "    \
"donc décider des coordonnées de la case libre à l'état initial. "      \
"Le jeu terminera n'importe où"                                         \

#define ROI "Ce profil de jeu est le plus complet. Vous êtes le roi ! " \
"Vos privilèges sont doubles : vous décidez des coordonnées de la case "\
"de départ, mais aussi de la case d'arrivée !"                          \


#define INIT3 "Enfin, vous serez invité à choisir le mode d'affichage. "\
"Ils sont au nombre de deux :"                                          \

#define NORMAL "Ce mode n'affiche pas les états intermédiaires. "       \
"L'affichage du chemin réussi final sera cepedant affiché."             \

#define COMPLET "Ce mode d'affichage va afficher tous les états "       \
" intermédiaires, que cette branche soit réussie ou non. "              \
" Attention, l'affichage risque de comporter beaucoup de lignes !"      \

void aide(const char *progname) {
  fputs("\n" "Module d'aide à l'utilisation de : ", stdout);
  fputs(progname, stdout);
  fputs("\n\n", stdout);

  fputs(WELCOME "\n", stdout);

  fputs(INIT "\n\n", stdout);
  fputs("BASIC : " BASIC "\n\n", stdout);
  fputs("AVANCE : " AVANCE "\n\n", stdout);
  fputs("EXPERT : " EXPERT "\n\n\n", stdout);

  fputs(INIT2 "\n\n", stdout);
  fputs("CHEVALIER : " CHEVALIER "\n\n", stdout);
  fputs("SEIGNEUR : " SEIGNEUR "\n\n", stdout);
  fputs("ROI : " ROI "\n\n\n", stdout);

  fputs(INIT3 "\n\n", stdout);
  fputs("NORMAL : " NORMAL "\n\n", stdout);
  fputs("COMPLET : " COMPLET "\n", stdout);
}
