#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "rebroussement.h"

// le tableau qui représente le plateau de jeu
int tab[LINE_NB][COL_NB];

// liste de mouvements
movlist *movements_list;

// nombre de mouvements
int n;

// coordonnées du point d'arrivée
int xa;
int ya;

// mode, profil et affichage de tous les mouvements ?
int mode = 0;
int profil = 0;
int affichage_mouvement = 0;

// nombre de chemins réussis trouvés
int found_trace = 0;

// Module relatif à la sauvegarde du plus court chemin
movlist *smallest_trace;
int smallest_tab[LINE_NB][COL_NB];
int smallest_mvt_nb = 0;
int smallest_coup_nb = 0;
int trace_nb = 0;

void start_backtraking() {
  // Mode Basic, Avancé ou Expert ?
  printf("Vous allez être solicité pour choisir le mode de jeu : ");
  printf("Basic, Avancé, Expert ou Expert2 ?\n");
  printf("- Pour le mode Basic entrez 1\n");
  printf("- Pour le mode Avancé entrez 2\n");
  printf("- Pour le mode Expert entrez 3\n");
  printf("- Pour le mode Expert2 (plateau différent) entrez 4\n");

  while ((scanf("%d", &mode)) == 1 &&
      (mode != 1 && mode != 2 && mode != 3 && mode != 4)) {
    printf ("Cette valeur ne correspond ni à 1, ni à 2, ni à 3, ni à 4.\n");
    printf ("Veuillez entrez une nouvelle valeur s'il vous plait.\n");
  }

  printf("=> ");
  switch(mode) {
  case 1:
    printf("Vous avez choisi le mode Basic\n");
    break;
  case 2:
    printf("Vous avez choisi le mode Avancé\n");
    break;
  case 3:
    printf("Vous avez choisi le mode Expert\n");
    break;
  default:
    printf("Vous avez choisi le mode Expert2\n");
  }

  if (mode == 2) {
    printf("Veuillez saisir le nombre de chemins que vous souhaitez mettre ");
    printf("en concurrence : (valeur inférieure à 10)\n");
    while ((scanf("%d", &trace_nb)) == 1 && (trace_nb > 10)) {
      printf ("Cette valeur ne doit pas dépasser 10.\n");
      printf ("Veuillez entrez une nouvelle valeur s'il vous plait.\n");
    }
  }
  printf("\n");

  // Profil Chevalier, Seigneur ou Roi ?
  printf("Vous allez être solicité pour choisir le profil : ");
  printf("Chevalier, Seigneur ou Roi ?\n");
  printf("- Pour le profil Chevalier entrez 1\n");
  printf("- Pour le profil Seigneur entrez 2\n");
  printf("- Pour le profil Roi entrez 3\n");

  while ((scanf("%d", &profil)) == 1
      && (profil != 1 && profil != 2 && profil != 3)) {
    printf ("Cette valeur ne correspond ni à 1, ni à 2, ni à 3.\n");
    printf ("Veuillez entrez une nouvelle valeur s'il vous plait.\n");
  }

  printf("=> ");
  switch(profil) {
  case 1:
    printf("Vous avez choisi le profil Chevalier\n");
    break;
  case 2:
    printf("Vous avez choisi le profil Seigneur\n");
    break;
  default:
    printf("Vous avez choisi le profil Roi\n");
  }
  printf("\n");

  // Affichage de tous les mouvements ?
  printf("Voulez vous voir l'affichage de TOUS les mouvements effectués ?\n");
  printf("Remarque : un bilan des mouvements du chemin correct sera affiché ");
  printf("à la fin dans les deux cas.\n");
  printf("- pour l'affichage normal entrez 0\n");
  printf("- pour l'affichage complet entrez 1\n");

  while ((scanf("%d", &affichage_mouvement)) == 1 && (affichage_mouvement != 0 && affichage_mouvement != 1)) {
    printf ("Cette valeur ne correspond ni à 0, ni à 1.\n");
    printf ("Veuillez entrez une nouvelle valeur s'il vous plait.\n");
  }

  printf("=> ");
  switch(affichage_mouvement){
  case 0:
    printf("Vous avez choisi l'affichage normal\n");
    break;
  default:
    printf("Vous avez choisi l'affichage complet\n");
  }
  printf("\n");

  // coordonnées du point de départ
  int x;
  int y;

  if (profil == 1) {
    x = 3;
    y = 3;
  } else {
    // Récupération des coordonnées de départ
    printf("Veuillez donner les coordonnées (x, y) que vous souhaitez pour ");
    printf("la case libre à l'étape initiale s'il vous plait :\n");
    printf("Remarque 1 : la première ligne/colonne porte le numéro 0\n");
    printf("Remarque 2 : x correspond au numéro de la ligne, y à la colonne\n");
    printf("Remarque 3 : donnez votre résultats sous la forme \"x y\" ");
    printf("puis appuyez sur la touche entré'\n");

    while ((scanf("%d", &x)) == 1 && (scanf("%d", &y)) == 1
      && !exist(x, y)) {
      printf ("Cette coordonnée n'est pas dans le plateau ! \n");
      printf ("Veuillez entrez une nouvelle coordonnée. \n");
    }

    if (profil == 3) {
      // Récupération des coordonnées d'arrivée
      printf("\n");
      printf("Veuillez donner les coordonnées que vous souhaitez pour ");
      printf("la case occupée à l'étape finale s'il vous plait :\n");
      printf("Remarque : la nomenclature est la même que précédemment\n");

      while ((scanf("%d", &xa)) == 1 && (scanf("%d", &ya)) == 1
        && !exist(x, y)) {
        printf ("Cette coordonnée n'est pas dans le plateau ! \n");
        printf ("Veuillez entrez une nouvelle coordonnée. \n");
      }
    }
  }

  // initialisation du tableau avec (x, y) LIBRE
  for (int i = 0; i < COL_NB; ++i) {
    for (int j = 0; j < LINE_NB; ++j) {
      tab[i][j] = OCCUPE;
    }
  }
  tab[x][y] = LIBRE;

  if (mode == 4) {
    for (int i = 0; i < COL_NB; ++i) {
      for (int j = 0; j < LINE_NB; ++j) {
        tab[i][j] = LIBRE;
      }
    }
    tab[x][y] = LIBRE;
    tab[1][3] = OCCUPE;
    tab[2][4] = OCCUPE;
    tab[2][5] = OCCUPE;
    tab[3][0] = OCCUPE;
    tab[3][1] = OCCUPE;
  }

  // Affichage de l'état initial
  printf("\nÉtat initial du plateau : \n");
  affichage(tab);
  printf("Calcul en cours... Veuillez patienter.\n\n");

  movements_list = movlist_empty();

  // on lance la recherche
  rebroussement();

  // gestion du cas où le mode est 3
  if ((mode == 3 || mode == 4) && found_trace > 0) {
    // on connait le chemin le plus court
    printf("\n\n");
    printf("Bravo ! Nous avons réussi à trouver le chemin le plus court ");
    printf("du plateau !\n");
    printf("Vous avez réussi à battre le solitaire chinois !");
    printf("\n");
    printf("Voici un récapitulatif du chemin le plus court : ");
    printf("Tout d'abbord le plateau :\n");
    affichage(smallest_tab);
    movlist_print(smallest_trace);
    int num = smallest_mvt_nb;
    int coups = smallest_coup_nb;
    printf("Le jeu s'est terminé avec un chemin de %d mouvements\n", num);
    printf("Ces %d mouvements représentent %d coups.\n\n", num, coups);
  }

  // la recherche est terminée
  printf("\nExécution du programme terminée\n");
  if (found_trace == 0) {
    printf("Il n'existe pas de solution à ce plateau.\n");
  }
}

void rebroussement() {
  // initialisation de la liste de choix
  slist *list = make_list(tab);

  int a, b, c, d, e, f;
  int dir;

  if (slist_isempty(list)) {
    // on récupère les coordonnées du dernier mouvement.
    movlist_head_values(movements_list, &a, &b, &e, &f);
    // on calcule la direction du mouvement précédent
    dir = which_dir(a, b, e, f);
    coo_move(a, b, &a, &b, &c, &d, &e, &f, dir);
    //on met à jour le tableau.
    tab[a][b] = OCCUPE;
    tab[c][d] = OCCUPE;
    tab[e][f] = LIBRE;
    // on décrémente le nombre de mouvements
    n = n - 1;
    movlist_remove_head(movements_list);
  } else {
    while (!finish(tab) && !slist_isempty(list)) {
      int x;
      int y;

      // on récupère les éléments de la celulle en tête :
      // le choix suivant de la liste.
      slist_head_values(list, &x, &y, &dir);

      //printf("--- Rang %d ---\n\n", n);
      // slist_print(liste);

      // on supprime la cellule
      slist_remove_head(list);

      // on calcule les coordonnées du nouveau mouvement

      // coo_move ne s'intéresse pas à la
      // présence au sein du plateau des points
      // de coordonnées (c,d) et (e,f), mais si (a,b)
      // est dans la liste avce la direction dir,
      // c'est que le mouvement (a, b, dir) nous emmene
      // sur une case :
      // 1- qui est dans le plateau et libre
      // 2- avec une case sauté qui était occupée
      // de coordonnées (c,d).
      // nous pouvons donc continuer sans soucis.
      coo_move(x, y, &a, &b, &c, &d, &e, &f, dir);

      // on insère le nouveau mouvement dans la liste.
      movlist_insert_head(movements_list, a, b, e, f);

      // on met à jour le tableau.
      tab[a][b] = LIBRE;
      tab[c][d] = LIBRE;
      tab[e][f] = OCCUPE;

      // on incrémente le nombre de mouvements
      n = n + 1;

      if (affichage_mouvement == 1) {
        printf("Mouvement provoqué : %d,%d %d,%d %d,%d\n\n", a, b, c, d, e, f);
      }

      if (!finish(tab)) {
          // appel récursif.
          rebroussement(n);
          if (slist_isempty(list) && !finish(tab)) {
              // on récupère les coordonnées du dernier mouvement.
              movlist_head_values(movements_list, &a, &b, &e, &f);
              // on calcule la direction du mouvement précédent
              dir = which_dir(a, b, e, f);
              coo_move(a, b, &a, &b, &c, &d, &e, &f, dir);

              movlist_remove_head(movements_list);

              // on met à jour le tableau.
              tab[a][b] = OCCUPE;
              tab[c][d] = OCCUPE;
              tab[e][f] = LIBRE;

              // on décrémente le nombre de mouvements
              n = n - 1;
          }
      } else {
        found_trace = found_trace + 1;
        int num = 0;
        int coups = compute_movement_number(movements_list);

        if (mode == 1) {
          printf("Bravo ! Un résultat a été trouvé\n");
          printf("Vous avez réussi à battre le solitaire chinois !\n");
          printf("\n");
          printf("Voici l'état du plateau à la fin de la partie :\n");
          affichage(tab);
          printf("\n");
          movlist_print(movements_list);
          printf("\n");
          printf("Le jeu s'est terminé avec un chemin de %d mouvements\n", n);
          printf("Ces %d mouvements représentent %d coups.\n", n, coups);
        } else {
          printf("\nVoici le chemin n°%d trouvé\n", found_trace);
          movlist_print(movements_list);
          printf("\n");
          save_result(coups);

          if (mode == 2) {
            if (found_trace < trace_nb) {
              // on recule d'un cran
              // on récupère les coordonnées du dernier mouvement
              movlist_head_values(movements_list, &a, &b, &e, &f);
              // on calcule la direction du mouvement précédent
              dir = which_dir(a, b, e, f);
              coo_move(a, b, &a, &b, &c, &d, &e, &f, dir);
              //on met à jour le tableau.
              tab[a][b] = OCCUPE;
              tab[c][d] = OCCUPE;
              tab[e][f] = LIBRE;
              // on décrémente le nombre de mouvements
              n = n - 1;
              movlist_remove_head(movements_list);
              num = smallest_mvt_nb;
              coups = smallest_coup_nb;
            } else {
              // on connait le chemin le plus court
              printf("\n\n");
              printf("Bravo ! Un chemin le plus court a été trouvé parmis ");
              printf("les %d premiers chemins\n", trace_nb);
              printf("Vous avez réussi à battre le solitaire chinois !");
              printf("\n\n");
              printf("Voici un récapitulatif du chemin le plus court : ");
              printf("Tout d'abbord le plateau :\n");
              affichage(smallest_tab);
              movlist_print(smallest_trace);
              num = smallest_mvt_nb;
              coups = smallest_coup_nb;
            }

            printf("Le jeu s'est terminé ");
            printf("avec un chemin de %d mouvements\n", num);
            printf("Ces %d mouvements représentent %d coups.\n\n", num, coups);
          }

          if (mode == 3 || mode == 4) {
            // on recule d'un cran
            // on récupère les coordonnées du dernier mouvement
            movlist_head_values(movements_list, &a, &b, &e, &f);
            // on calcule la direction du mouvement précédent
            dir = which_dir(a, b, e, f);
            coo_move(a, b, &a, &b, &c, &d, &e, &f, dir);
            //on met à jour le tableau.
            tab[a][b] = OCCUPE;
            tab[c][d] = OCCUPE;
            tab[e][f] = LIBRE;
            // on décrémente le nombre de mouvements
            n = n - 1;
            movlist_remove_head(movements_list);
            num = smallest_mvt_nb;
            coups = smallest_coup_nb;
          }
        }
      }
    }
  }
}

//--- fonction d'affichage
void affichage(int tab[LINE_NB][COL_NB]) {
  int i,j;
  for (i = 0; i < 7 ; i++) {
    for(j = 0 ; j < 7 ; j++) {
      if (!exist(i, j)) {
        printf("   ");
      } else {
        switch(tab[i][j]) {
        case LIBRE:
          printf(" L ");
          break;
        case OCCUPE:
          printf(" O ");
          break;
        }
      }
    }
    printf("\n");
  }
  printf("\n");
}

bool finish(int tab[LINE_NB][COL_NB]) {
    int n = 0;
    for (int x = 0; x < LINE_NB; ++x) {
        for (int y = 0; y < COL_NB; ++y) {
            if (exist(x, y)) {
                if (tab[x][y] == OCCUPE) {
                    n = n + 1;
                    if (n != 1) {
                      return false;
                    }
                }
            }
        }
    }

    if (profil == 3) {
        if (tab[xa][ya] == LIBRE) {
            return false;
        }
    }

    return true;
}

void save_result(int coup_number) {
  if (found_trace == 1 || coup_number < smallest_coup_nb) {
    // on enregistre le dernier chemin comme chemin le plus court
    movlist *tampon = movlist_empty();
    cmovlist *p = movements_list -> head;
    while (p != NULL) {
      movlist_insert_head(tampon, p -> x1, p -> y1, p -> x2, p -> y2);
      p = p -> next;
    }
    smallest_trace = movlist_empty();
    cmovlist *p2 = tampon -> head;
    while (p2 != NULL) {
      movlist_insert_head(smallest_trace,
          p2 -> x1, p2 -> y1, p2 -> x2, p2 -> y2);
      p2 = p2 -> next;
    }

    for (int i = 0; i < COL_NB; ++i) {
      for (int j = 0; j < LINE_NB; ++j) {
        smallest_tab[i][j] = tab[i][j];
      }
    }

    smallest_mvt_nb = n;
    smallest_coup_nb = coup_number;
  }
}
