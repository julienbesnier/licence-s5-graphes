#include <stdbool.h>

#include "./mobility/make_list.c"
#include "./movements/movements.c"


// start_backtraking : lance rebroussement après avoir initialisé le tableau.
extern void start_backtraking();

// rebroussement : fonction de rebroussement, fonction principale du jeu.
//    Si mode vaut 1 :
//    Recherche un chemin réussi. S'arrête quand ce chemin est trouvé.
//    Il s'agit du premier chemin trouvé et non du plus court.
//
//    Si mode vaut 2 :
//    Recherche n chemins réussis. S'arrête ces n chemins sont trouvés.
//    Affiche le chemin le plus court trouvé en nombre de coups.
//    Si plusieurs chemins ont même nombre de coups (et qu'il est minimal),
//    affichera toujours le chemin trouvé en premier.
extern void rebroussement();

// affichage : élabore l'affichage du plateau
extern void affichage(int tab[LINE_NB][COL_NB]);

// finish : indique si le plateau sans son état actuel correspond à une partie réussie.
extern bool finish(int tab[LINE_NB][COL_NB]);

// save_result : enregistre le dernier chemin trouvé si son nombre de coups est inférieur
//    à celui déjà mémorisé. Si il n'y avait pas encore de coup enregistré,
//    enregistre le dernier coup.
extern void save_result(int coup_number);
